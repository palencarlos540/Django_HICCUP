from ast import Try
from django.shortcuts import render
from django.http.response import JsonResponse

# Create your views here.
def pending_user_list(request):
    userlist = []
    for i in range(100):
        userlist.extend([{
            'id': i,
            'username': "name" + str(i + 1),
            'email': "email" + str(i + 1)
        }])
        
    row = 10
    start = 1
    try:
        page = int(request.GET['page'])
    except:
        page = 1
    start = row * (page - 1)
    
    count = 100
    
    end = min(count, start + row - 1)
    userlist = userlist[start:end + 1]

    returnData = {
        'count': count,
        'page': page,
        'userlist': userlist
    }

    if request.method=='GET':
        return JsonResponse(returnData)